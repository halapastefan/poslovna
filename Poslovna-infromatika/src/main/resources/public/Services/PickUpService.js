(function(angular){

	var app = angular.module('app');

	app.service('PickUpService', function(){

		var vrednost;
		var column;
		var lookUpColumnName;
		var lookUpValue;
		var lookUpcolumn;

		this.getVrednost = function(){
			return vrednost;
		};

		this.setVrednost = function(value){
			vrednost = value;
		};

		this.getColumn = function(){
			return column;
		};

		this.setColumn = function(col){
			column = col;
		};
		this.setLookUpValue = function(value) {
			lookUpValue = value;
		};
		this.getLookUpValue = function() {
			return lookUpValue;
		};
		this.setLookUpColumn = function(column) {
			lookUpcolumn =column;
		};
		this.getLookColumn = function(){
			return lookUpcolumn;
		};

	});

})
(angular);
